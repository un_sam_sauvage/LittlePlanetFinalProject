using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class OxygenZone : MonoBehaviour
{
    public Collider[] colliderPlayer;
    private Collider[] playerInSideZone;
    private Collider[] playerOutSideZone;
    
    private Vector3 _origin;
    public float zoneRadius;
    void FixedUpdate()
    {
        _origin = transform.position;
        //the player's oxygen increases when he is inside a safe zone
        playerInSideZone = Physics.OverlapSphere(_origin, zoneRadius);
        foreach (var player in playerInSideZone)
        {
            if (player.CompareTag("Player"))
            {
                player.GetComponent<PlayerInfo>().IsOnSafeZone = true;
            }
        }
        //the player's oxygen descreases when he is outside a zone
        playerOutSideZone = colliderPlayer.Except(playerInSideZone).ToArray();
        foreach (var player in playerOutSideZone)
        {
            if (player.CompareTag("Player"))
            {
                player.GetComponent<PlayerInfo>().IsOnSafeZone = false;
            }
        }
    }
    private void OnDrawGizmosSelected()
    {
        //to display the safe zone
        Gizmos.color = Color.red;
        Debug.DrawLine(_origin,_origin);
        Gizmos.DrawWireSphere(_origin, zoneRadius);
    }
}
