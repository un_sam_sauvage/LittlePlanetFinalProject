using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInfo : MonoBehaviour
{
    public float oxygenValue;
    public float maxOxygen;

    public float multiplierTimeOutSide;
    public float multiplierTimeInSideSafeZone;
    
    public Text textOxygen;

    public bool IsOnSafeZone;
    private void Start()
    {
        oxygenValue = maxOxygen;
    }

    void Update()
    {
        textOxygen.text = Mathf.Round(oxygenValue).ToString();
        if (oxygenValue >= maxOxygen)
        {
            oxygenValue = maxOxygen;
        }
        if (IsOnSafeZone)
        {
            oxygenValue += Time.deltaTime * multiplierTimeInSideSafeZone;
        }
        else
        {
            oxygenValue -= Time.deltaTime * multiplierTimeOutSide;
        }
    }
}
