using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Log : MonoBehaviour
{
    public TextMeshProUGUI textComponent;
    [TextArea(5, 15)]
    public string[] lines;
    public float textSpeed;

    private int _indexText;

    private int _numberLog;

    private void Start()
    {
        textComponent.text = string.Empty;
        Debug.Log(lines[_indexText].Length);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            StartDialogue();
        }

        if (Input.GetMouseButtonDown(0))
        {
            if (textComponent.text == lines[_indexText])
            {
                NextLine();
            }
           
        }
    }

    private void StartDialogue()
    {
        _indexText = 0;
        StartCoroutine(TypeLine());
    }

    IEnumerator TypeLine()
    {
        foreach (char c in lines[_indexText])
        {
            textComponent.text += c;
            yield return new WaitForSeconds(textSpeed);
        }
    }

    private void NextLine()
    {
        if (_indexText < lines.Length - 1)
        {
            _indexText++;
            textComponent.text = string.Empty;
            StartCoroutine(TypeLine());
        }
        else
        {
            textComponent.gameObject.SetActive(false);
        }
    }
}
