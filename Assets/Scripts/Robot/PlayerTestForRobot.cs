using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTestForRobot : MonoBehaviour
{
    
    public float speed;
 
    void Update(){
        //On récupère les Axis Horizontal et Vertical qu'on attribue aux 2 floats correspondant
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        Vector3 playerMovement = new Vector3(horizontal, 0f, vertical) * (speed * Time.deltaTime);
        transform.Translate(playerMovement, Space.Self);    //on applique le translate aux coordonnées locales du player
    }
}
